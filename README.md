# saludmental

En un alarde de transparencia democrática, el Gobierno de España; al ser preguntado por los datos de salud mental, contesta con un fichero pdf de 700 páginas.

En este repositorio, hay script para parsear dicho fichero y convertirlo en csv.

Adrmás, a través de Jupiter Notebooks, analizamos dichos datos.

## Cómo lanzar el notebook

Crear entorno virtual python e instalar dependencias

     python -m venv; ./venv/bin/activate; pip install -r requirements.txt

Lanzar jupiter notebook:

     jupyter notebook Estadísticas.ipynb


## Descargar PDF original
https://gitlab.com/adrianmz/saludmental/-/raw/main/IniciativaGobierno.pdf?inline=false
