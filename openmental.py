#!/bin/env python3

import re
import csv

from py_pdf_parser.loaders import load_file


doc = load_file("IniciativaGobierno.pdf")

r_diag = re.compile(r"^(F\d{2}(\.\d{1,2})?)")
r_cnae = re.compile(r"^([0A-Z]{1})$")
r_age = re.compile(r"^(\d{2}-\d{2})|Otros")


def get_elements_by_year(year):
    start = doc.elements.filter_by_text_equal("AÑO {}".format(year))
    if len(start) != 1:
        raise Exception("year not found %d" % year)

    end = doc.elements.filter_by_text_equal("AÑO {}".format(year + 1))
    if len(end) > 0:
        return doc.elements.between(start[0], end[0])
    return doc.elements.after(start[0])

def main():
    data = parse_file()
    with open('salud_mental.csv', 'w', newline='') as output_file:
        dict_writer = csv.DictWriter(output_file, data[0].keys())
        dict_writer.writeheader()
        dict_writer.writerows(data)


def parse_file():
    total_data = []
    for year in range(2018, 2022):
        data = get_elements_by_year(year)
        index = get_data(year, data)
        if year == 2019:
            # corrections
            index[0]["diag"] = "F01"
            index[1]["diag"] = "F01.5"
        if year == 2020:
            # corrections
            index[0]["diag"] = "F01.5"

        numbers = get_numbers(data)
        if len(index) != len(numbers):
            raise Exception("lengths do not match")
        for i, idx in enumerate(index):
            idx["mean"] = numbers[i]
            total_data.append(idx)

    return total_data


def get_numbers(data):
    datum = []
    nums = data.filter_by_font('ArialMT,6.0')
    if len(nums) == 0:
        nums = data.filter_by_font('ArialMT,5.9') # 2021 has 5.9
    for n in nums:
        if "Duración" in n.text():
            print(n.text())
            continue
        for x in n.text().split("\n"):
            datum.append(float(x.replace(",", ".")))
    return datum


def get_data(year, data):
    result = []
    age = None
    genero = None
    situation = None
    diag = None
    cnae = None
    for d in data:
        if r_diag.match(d.text()):
            #print("Diagnosis: %s" % d.text())
            diag = d.text()
        if r_cnae.match(d.text()):
            #print("cnae: %s" % d.text())
            cnae = d.text()
        if r_age.match(d.text()):
            #print("age %s" % d.text())
            age = d.text()
            # contiene hombre/mujer
            if "HOMBRE" in d.text() or "MUJER" in d.text():
                # simple case, next is the sutiation and continue to next line
                age, genero = d.text().split(" ")
                situation = float(data.after(d)[0].text())
                result.append(
                    {
                        "year": year,
                        "diag": diag,
                        "cnae": cnae,
                        "age": age,
                        "genero": genero,
                        "situation": situation,
                    }
                )
        if d.text() in ["HOMBRE", "MUJER"]:
            situation = float(data.after(d)[0].text())
            result.append(
                {
                    "year": year,
                    "diag": diag,
                    "cnae": cnae,
                    "age": age,
                    "genero": d.text(),
                    "situation": situation,
                }
            )
    return result


if __name__ == "__main__":
    main()
